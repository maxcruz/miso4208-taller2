module.exports = { 
  'Los estudiantes teacher page filter': function(browser) {
    browser
      .resizeWindow(1280, 1024)
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .pause(1000)
      .waitForElementVisible('.profesor', 4000)
      .click('.profesor > a')
      .pause(1000)
      .waitForElementVisible('.materias', 4000)
      .click('.materias > div > .labelHover')
      .pause(1000)
      .assert.containsText('.cursiveText.collapse.in','en las materias seleccionadas')
      .end();
  }
};
